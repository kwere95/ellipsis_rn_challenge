/**
 * @format
 * @flow strict-local
 */
 import 'react-native-gesture-handler';
import React from 'react';
import NavigationScreen from './components/Navigation';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const App = () => {
  return (
    <SafeAreaProvider>
      <NavigationScreen />
    </SafeAreaProvider>
  );
};

export default App;
