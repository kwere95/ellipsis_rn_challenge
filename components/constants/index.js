export const colors = {
  white: '#fff',
  gray: 'gray',
  deepGray: '#484848',
  kijani: '#00CA2F',
  kijivu: '#373A4C',
  brown: '#A86A15',
};
