/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
} from 'react-native';
import {Header} from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {useFocusEffect} from '@react-navigation/native';
import Ionic from 'react-native-vector-icons/Ionicons';
import {colors} from '../constants';

const OneProduct = ({navigation, route}) => {
  const {product} = route.params;
  const [num, setNum] = React.useState(0);
  const [items, setItems] = React.useState(null);
  const [isAvailable, setIsAvailable] = React.useState(false);

  useFocusEffect(
    React.useCallback(() => {
      AsyncStorage.getItem('@products').then(pro => {
        if (pro !== null) {
          setItems(JSON.parse(pro));
          const data = JSON.parse(pro);
          setNum(JSON.parse(pro).length);
          if (data) {
            data.forEach(e => {
              if (e.id === product.id) {
                setIsAvailable(true);
              }
            });
          }
        }
      });
    }, [product.id]),
  );

  const addToCart = async () => {
    try {
      let list = [];
      const value = await AsyncStorage.getItem('@products');
      if (value !== null) {
        list = JSON.parse(value);
        list.push({id: product.id});
        await AsyncStorage.removeItem('@products');
      } else {
        list.push({id: product.id});
      }
      await AsyncStorage.setItem('@products', JSON.stringify(list));
      Alert.alert('Added Successfuly');
      return setItems(list);
    } catch (error) {
      console.log(error);
    }
  };

  const removeFromCart = async () => {
    try {
      let list = [];
      const value = await AsyncStorage.getItem('@products');
      if (value !== null) {
        list = JSON.parse(value);
        list = list.filter(item => item.id !== product.id);
      } else {
        Alert.alert('Your cart is empty');
      }
      await AsyncStorage.setItem('@products', JSON.stringify(list));
      Alert.alert('Successfuly deleted');
      return setItems(list);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Header
        containerStyle={{
          borderBottomWidth: 0,
          paddingHorizontal: 20,
          backgroundColor: '#f5f5f5',
        }}
        backgroundColor="transparent"
        barStyle="dark-content"
        leftComponent={
          <TouchableOpacity>
            <Ionic
              name="arrow-back-circle"
              size={35}
              color={colors.kijivu}
              onPress={() => navigation.goBack()}
            />
          </TouchableOpacity>
        }
        rightComponent={
          <TouchableOpacity
            style={styles.align}
            onPress={() => navigation.navigate('Cart')}>
            <Ionic name="cart" size={30} color={colors.kijivu} />
            <Text style={styles.badge}>{num}</Text>
          </TouchableOpacity>
        }
      />

      <View
        style={{
          flex: 1,
          paddingHorizontal: 20,
          paddingTop: 10,
          paddingBottom: 30,
          backgroundColor: '#f5f5f5',
        }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.txt}>{product.title}</Text>
          <Text
            style={{...styles.txt, color: colors.kijani, marginVertical: 10}}>
            $ {product.price}
          </Text>

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginVertical: 10,
            }}>
            <View
              style={{
                width: wp('90%'),
                height: wp('90%'),
                backgroundColor: '#fff',
                padding: 30,
                borderRadius: 20,
              }}>
              <Image
                style={{width: '100%', height: '100%', overflow: 'hidden'}}
                source={{
                  uri: product.image,
                }}
                resizeMode="contain"
              />
            </View>
          </View>
          <View>
            <Text
              style={{
                fontSize: 16,
                fontWeight: 'bold',
                color: colors.kijivu,
                marginVertical: 10,
              }}>
              Description
            </Text>
            <Text
              style={{
                color: colors.kijivu,
                fontSize: 16,
                textAlign: 'justify',
              }}>
              {product.description}
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: 50,
              marginBottom: 10,
            }}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                color: colors.kijani,
                marginVertical: 10,
              }}>
              $ {product.price}
            </Text>
            {isAvailable ? (
              <TouchableOpacity
                onPress={removeFromCart}
                style={{...styles.btn, backgroundColor: colors.brown}}>
                <Text
                  style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                  Remove from Cart
                </Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={addToCart} style={styles.btn}>
                <Text
                  style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
                  Add to Cart
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  txt: {
    color: colors.kijivu,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },

  btn: {
    backgroundColor: 'black',
    padding: 15,
    width: '60%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  align: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  badge: {
    backgroundColor: colors.kijani,
    paddingHorizontal: 5,
    borderRadius: 5,
  },
});

export default OneProduct;
