/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Header} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Ionic from 'react-native-vector-icons/Ionicons';
import {FlatGrid} from 'react-native-super-grid';
import {useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {colors} from '../constants';
import {Product} from '../reusable';
import data from '../constants/data';

const ProductScreen = ({navigation}) => {
  const [num, setNum] = React.useState(0);

  useFocusEffect(
    React.useCallback(() => {
      AsyncStorage.getItem('@products').then(pro => {
        if (pro !== null) {
          setNum(JSON.parse(pro).length);
        }
      });
    }, []),
  );

  return (
    <>
      <Header
        containerStyle={{
          borderBottomWidth: 0,
          paddingHorizontal: 20,
          backgroundColor: '#f5f5f5',
        }}
        backgroundColor="transparent"
        barStyle="dark-content"
        leftComponent={
          <TouchableOpacity>
            <Ionic
              name="arrow-back-circle"
              size={35}
              color={colors.kijivu}
              onPress={() => navigation.goBack()}
            />
          </TouchableOpacity>
        }
        centerComponent={
          <Text
            style={{
              color: colors.kijivu,
              fontWeight: 'bold',
              fontSize: 20,
              marginTop: 5,
            }}>
            Products
          </Text>
        }
        rightComponent={
          <TouchableOpacity
            style={styles.align}
            onPress={() => navigation.navigate('Cart')}>
            <Ionic name="cart" size={30} color={colors.kijivu} />
            <Text style={styles.badge}>{num}</Text>
          </TouchableOpacity>
        }
      />
      <View
        style={{
          flex: 1,
          paddingHorizontal: 5,
          paddingBottom: 30,
          backgroundColor: '#f5f5f5',
        }}>
        <FlatGrid
          itemDimension={wp('30%')}
          data={data}
          style={styles.gridView}
          spacing={10}
          renderItem={({item}) => (
            <Product
              onPress={() => navigation.navigate('One', {product: item})}
              uri={item.image}
              title={item.title}
              desc={item.description}
              price={item.price}
            />
          )}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  align: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  badge: {
    backgroundColor: colors.kijani,
    paddingHorizontal: 5,
    borderRadius: 5,
  },
});

export default ProductScreen;
