/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Header} from 'react-native-elements';
import Ionic from 'react-native-vector-icons/Ionicons';
import {colors} from '../constants';
import {MyInput, MyButton} from '../reusable';

const LogInScreen = ({navigation}) => {
  return (
    <>
      <Header
        containerStyle={{
          borderBottomWidth: 0,
        }}
        backgroundColor="transparent"
        barStyle="dark-content"
      />

      <View style={styles.container}>
        <View>
          <View style={styles.align}>
            <Text style={{...styles.texts, fontSize: 25}}>SIGN IN</Text>
            <TouchableOpacity style={styles.align}>
              <Text style={{...styles.texts, fontSize: 16, marginRight: 10}}>
                Sign Up
              </Text>
              <Ionic
                name="arrow-forward-outline"
                size={20}
                color={colors.kijivu}
              />
            </TouchableOpacity>
          </View>

          <View style={{marginVertical: 50}}>
            <Text style={styles.label}>USERNAME</Text>
            <MyInput maxLength={20} placeholder="name" />
            <Text style={{...styles.label, marginTop: 10}}>PASSWORD</Text>
            <MyInput
              maxLength={20}
              secureTextEntry={true}
              placeholder="password"
            />
          </View>
        </View>

        <View>
          <View style={{marginBottom: 20}}>
            <MyButton
              title="SIGN IN"
              onPress={() => navigation.navigate('Products')}
            />
          </View>
          <Text
            style={{
              color: colors.kijivu,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            Signin with social account
          </Text>
          <View
            style={{
              ...styles.align,
              justifyContent: 'space-evenly',
              marginTop: 20,
            }}>
            <Ionic name="logo-google" size={20} color={colors.kijivu} />
            <Ionic name="logo-facebook" size={20} color={colors.kijivu} />
            <Ionic name="logo-twitter" size={20} color={colors.kijivu} />
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 50,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  texts: {
    color: colors.deepGray,
    fontWeight: 'bold',
  },
  align: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  label: {
    color: colors.deepGray,
    fontWeight: 'bold',
    opacity: 0.6,
    marginVertical: 3,
  },
});

export default LogInScreen;
