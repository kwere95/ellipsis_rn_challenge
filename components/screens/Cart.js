/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Alert,
} from 'react-native';
import {Header} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ionic from 'react-native-vector-icons/Ionicons';
import {colors} from '../constants';
import data from '../constants/data';

const CartScreen = ({navigation}) => {
  const [items, setItems] = React.useState([]);

  React.useEffect(() => {
    let list = [];
    AsyncStorage.getItem('@products').then(pro => {
      if (pro !== null) {
        JSON.parse(pro).forEach(e => {
          data.forEach(da => {
            if (da.id === e.id) {
              list.push(da);
            }
          });
        });
        setItems(list);
      }
    });
  }, []);

  const removeFromCart = async id => {
    try {
      let list = [];
      let list2 = [];
      const value = await AsyncStorage.getItem('@products');
      if (value !== null) {
        list = JSON.parse(value);
        list = list.filter(item => item.id !== id);
      } else {
        Alert.alert('Your cart is empty');
      }
      await AsyncStorage.setItem('@products', JSON.stringify(list));
     

      list.forEach(e => {
        data.forEach(da => {
          if (da.id === e.id) {
            list2.push(da);
          }
        });
      });

      return setItems(list2);
    } catch (error) {
      console.log(error);
    }
  };

  const getTotal = () => {
    let sum = 0;
    items.forEach(item => {
      sum = sum + item.price;
    });
    return sum.toFixed(2);
  };

  console.log(items);

  return (
    <>
      <Header
        containerStyle={{
          borderBottomWidth: 0,
          paddingHorizontal: 20,
          backgroundColor: '#f5f5f5',
        }}
        backgroundColor="transparent"
        barStyle="dark-content"
        leftComponent={
          <TouchableOpacity>
            <Ionic
              name="arrow-back-circle"
              size={35}
              color={colors.kijivu}
              onPress={() => navigation.goBack()}
            />
          </TouchableOpacity>
        }
        centerComponent={
          <Text
            style={{
              color: colors.kijivu,
              fontWeight: 'bold',
              fontSize: 20,
              marginTop: 5,
            }}>
            My Cart
          </Text>
        }
      />

      <View
        style={{
          flex: 1,
          paddingHorizontal: 20,
          paddingTop: 10,
          paddingBottom: 30,
          backgroundColor: '#f5f5f5',
        }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          {items.map(item => (
            <View key={item.id} style={{flexDirection: 'row'}}>
              <View
                style={{
                  width: wp('40%'),
                  height: wp('40%'),
                  backgroundColor: '#fff',
                  padding: 10,
                  borderRadius: 20,
                  marginRight: 10,
                  marginBottom: 20,
                }}>
                <Image
                  style={{width: '100%', height: '100%', overflow: 'hidden'}}
                  source={{
                    uri: item.image,
                  }}
                  resizeMode="contain"
                />
              </View>
              <View
                style={{
                  width: wp('35%'),
                  marginVertical: 10,
                }}>
                <Text style={{...styles.text, fontSize: 16}}>{item.title}</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 30,
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      ...styles.text,
                      fontSize: 20,
                      color: colors.kijani,
                    }}>
                    $ {item.price}
                  </Text>
                  <TouchableOpacity
                    onPress={() => removeFromCart(item.id)}
                    style={{
                      paddingHorizontal: 12,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text style={{...styles.text, fontSize: 20, color: 'red'}}>
                      x
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ))}

          <View style={{marginTop: 35}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={{...styles.text, fontSize: 18}}>Total:</Text>
              <Text style={{...styles.text, fontWeight: 'bold', fontSize: 20}}>
                $ {getTotal()}
              </Text>
            </View>
            <TouchableOpacity
              style={{
                backgroundColor: 'black',
                borderRadius: 5,
                marginTop: 20,
                marginBottom: 5,
                alignItems: 'center',
                justifyContent: 'center',
                padding: 15,
              }}>
              <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>
                Checkout
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    color: colors.kijivu,
    fontWeight: 'bold',
  },
});

export default CartScreen;
