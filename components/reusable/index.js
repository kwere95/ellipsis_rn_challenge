/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Feather from 'react-native-vector-icons/Feather';
import {colors} from '../constants';

export const MyInput = props => {
  return (
    <>
      <TextInput
        style={{...styles.majorInput, ...props.inputStyles}}
        keyboardType={props.keyboardType}
        maxLength={props.maxLength}
        onChangeText={props.onChangeText}
        secureTextEntry={props.secureTextEntry}
        placeholder={props.placeholder}
      />
    </>
  );
};

export const MyButton = props => {
  return (
    <>
      <TouchableOpacity
        onPress={props.onPress}
        style={{...styles.majorBtn, ...props.btnStyles}}>
        <Text style={{...styles.BtnTitle, ...props.btnTitleStyles}}>
          {props.title}
        </Text>
      </TouchableOpacity>
    </>
  );
};

export const Product = props => {
  return (
    <>
      <TouchableOpacity
        onPress={props.onPress}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#fff',
          padding: 15,
          borderRadius: 10,
        }}>
        <View style={{width: wp('30%'), height: wp('30%'), borderRadius: 50}}>
          <Image
            style={{width: '100%', height: '100%', overflow: 'hidden'}}
            source={{
              uri: props.uri,
            }}
            resizeMode="stretch"
          />
        </View>

        <Text
          style={{
            ...styles.txt,
            fontWeight: 'bold',
            fontSize: 16,
            marginVertical: 8,
          }}>
          {props.title}
        </Text>
        <Text style={{...styles.txt, opacity: 0.8}}>
          {props.desc.substring(0, 30)}...
        </Text>
        <View style={styles.line} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: '100%',
          }}>
          <Text style={{...styles.txt, fontWeight: 'bold', fontSize: 20}}>
            $ {props.price}
          </Text>
          <Feather
            style={{
              backgroundColor: colors.kijani,
              padding: 5,
              borderRadius: 3,
            }}
            name="shopping-bag"
            size={15}
            color={colors.white}
          />
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  majorInput: {
    fontSize: 18,
    paddingVertical: 8,
    paddingHorizontal: 10,
    color: '#000000',
    fontFamily: 'FiraSans-Medium',
    backgroundColor: '#ddd',
    borderRadius: 5,
  },
  majorBtn: {
    backgroundColor: colors.kijivu,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 14,
    borderRadius: 5,
  },
  BtnTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  txt: {
    color: colors.kijivu,
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: '#eee',
    marginVertical: 10,
  },
});
